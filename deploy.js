define([
    'amber/deploy',
    // --- packages to be deployed begin here ---
    'domite/DOMite'
    // --- packages to be deployed end here ---
], function (amber) {
    return amber;
});
